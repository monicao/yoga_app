class Schedule < ActiveRecord::Base
  validates :day, presence: true
  validates :name, presence: true
  validates :start_time, presence: true
  validates :end_time, presence: true
end