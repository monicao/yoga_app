# Homepage (Root path)
helpers do
  # Returns
  #   nil if not logged in
  #   current user object if logged in
  def current_user
    # runs for every request
    @current_user ||= User.find_by(id: session[:user_id])
    # the above is the same as
    # unless @current_user
    #   @current_user = User.find_by(id: session[:user_id])
    # end
    # @current_user
  end
end
get '/' do
  # app/views/layout.erb <-- layout file
  # app/views/index.erb  <-- view file

  # get some data from the database
  # store it in an instance variable
  @schedule_items = Schedule.all

  session[:random] = "browser number #{(rand * 1000).floor}"
  # Don't store entire objects (they have to be serialized and that's a pain in the ass)
  session[:user] = User.first
  # Store references
  session[:user_id] = User.first.id

  # the view accesses instance variable to display page
  erb :index
end

get '/other' do
  if current_user
    erb :other
  else
    redirect '/login'
  end
end





