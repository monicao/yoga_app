Sinatra
=============

Yoga Schedule App

### Data Model

```
Schedule
  - day: string "Monday", "Tuesday"
  - name: string "Hatha Level 2"
  - instructor: string "Jane Smith"
  - start_time: datetime
  - end_time: datetime
```
  
Brought to you by Lighthouse Labs

## Getting Started

1. `bundle install`
2. `shotgun -p 3000 -o 0.0.0.0`
3. Visit `http://localhost:3000/` in your browser
