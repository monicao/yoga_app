class CreateSchedules < ActiveRecord::Migration
  def change
    create_table :schedules do |t|
      t.string :day
      t.string :name
      t.string :instructor
      t.timestamp :start_time
      t.timestamp :end_time
    end
  end
end
